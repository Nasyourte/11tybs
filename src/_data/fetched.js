module.exports = async function() {
    return await fetch("https://evilinsult.com/generate_insult.php?lang=en&type=json")
        .then( r => r.json())
        .then( o => o)
};