---
title: Hello
---
this part is from `index.md`

# here is some content

This is the default nunjucks based layout.

You can use [webC based layout](/web-component)

## Examples

Voyage [example with webC components](/voyage-homepage)

[you can also use shoelace template](/shoelace)

## _data
[Data based page generation](/person-0)
The JSON files goes in the `_data` folder