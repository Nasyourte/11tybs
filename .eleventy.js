const CleanCSS = require("clean-css");
const { minify } = require("terser");
const pluginWebc = require("@11ty/eleventy-plugin-webc");
const pluginBundle = require("@11ty/eleventy-plugin-bundle");
const { EleventyRenderPlugin } = require("@11ty/eleventy");

module.exports = function(eleventyConfig) {
    // statics files
    eleventyConfig.addPassthroughCopy("src/static/*");
    eleventyConfig.addPassthroughCopy({"node_modules/@shoelace-style/shoelace/dist/": "sl"})
    // filters
    eleventyConfig.addFilter("cssmin", function(code) {
        return new CleanCSS({}).minify(code).styles;
    });
    eleventyConfig.addNunjucksAsyncFilter("jsmin", async function (code, callback) {
        try {
            const minified = await minify(code);
            callback(null, minified.code);
        } catch (err) {
            console.error("Terser error: ", err);
            callback(null, code);
        }
    });
    // webC
    eleventyConfig.addPlugin(pluginWebc, {
        components: "src/_components/*.webc"
    });
    eleventyConfig.addPlugin(pluginBundle, {
        transforms: [
            async function (content) {
                if (this.type === "js" /*&& process.env.ELEVENTY_ENV === "production"*/) {
                    const minified = await minify(content);
                    return minified.code;
                }
                return content;
            },
        ],
    });
    eleventyConfig.ignores.add("**/_components/**");
    // renderer
    eleventyConfig.addPlugin(EleventyRenderPlugin);

    return {
        dir: {
            input: "src",
            output: "_site"
        }
    }
}